
#ifndef STRUCTS_H
# define STRUCTS_H

# define TRUE 1
# define FALSE 0
# define BYTE unsigned char

# include <stdio.h>
# include <stdlib.h>
# include <string.h>

typedef struct      s_args
{
	int             help;
	int             ports[2];
	char            *ip;
	char            *file;
	int             speedup;
	char            *scans[6];
}                   t_args;

# define MAKE_ARGS() ((t_args) {0, {1, 1024}, 0, 0, 0, {0, 0, 0, 0, 0, 0}})
# define STR_EQUAL(s1, s2) (strcmp(s1, s2) ? FALSE : TRUE)

#endif