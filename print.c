#include <ft_nmap.h>

/*
 * Prints the usage info
 */
int     print_usage(void)
{
	printf("ft_nmap [--help] [--ports [NUMBER/RANGE]] --ip IP ADDRESS [" \
			       "--speedup [NUMBER]] [--scan [TYPE]]\nOr\n");
	printf("ft_nmap [--help] [--ports [NUMBER/RANGE]] --file FILE " \
			       "[--speedup [NUMBER]] [--scan [TYPE]]");
	return (1);
}

/*
 * Prints the --help info
 */
int     print_help(void)
{
	printf("Help Screen\n");
	printf("ft_nmap [OPTIONS]\n");
	printf("--help Print this help screen\n");
	printf("--ports ports to scan (eg: 1-10 or 1,2,3 or 1,5-15)\n");
	printf("--ip ip addresses to scan in dot format\n");
	printf("--file File name containing IP addresses to scan,\n");
	printf("--speedup [250 max] number of parallel threads to use\n");
	printf("--scan SYN/NULL/FIN/XMAS/ACK/UDP\n");
	return (2);
}

/*
 * Prints t_args struct
 */
void    print_args(t_args *args)
{
	printf("--help : %d\n", args->help);
	printf("--ports : %d - %d\n", args->ports[0], args->ports[1]);
	printf("--ip : %s\n", args->ip);
	printf("--file : %s\n", args->file);
	printf("--speedup : %d\n", args->speedup);
	printf("--scans : %s, %s, %s, %s, %s, %s\n", args->scans[0], \
			args->scans[1], args->scans[2], args->scans[3], args->scans[4], \
			args->scans[5]
	);
}

int     print_failure(int ret, char *msg)
{
	printf("Error : %s\n");
	return (ret);
}