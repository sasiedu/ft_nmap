#include <ft_nmap.h>

/*
 * args - pointer to current args structure
 * av - is a pointer to the current string in the argument string array
 * next - i s a pointer to the next string in the argument string array
 */
t_args      *load_no_param_arg(t_args *args, char **av)
{
	if (av == NULL || *av == NULL)
		return (args);
	if (STR_EQUAL(*av, "--help"))
		args->help = TRUE;
	av++;
	return (args);
}

t_args      *load_single_param_arg(t_args *args, char **av, char **next)
{
	if (av == NULL || *av == NULL || next == NULL)
		return (args);
	if (STR_EQUAL(*av, "--ip"))
		args->ip = *next;
	else if (STR_EQUAL(*av, "--file"))
		args->file = *next;
	else if (STR_EQUAL(*av, "--speedup") && !is_string_number(*next))
		EXIT(print_failure(1, "--speedup takes an integer between 0 - 250"));
	else if (STR_EQUAL(*av, "--speedup") && is_string_number(*next))
		args->speedup = atoi(next) > 250 ? 0 : atoi(next);
	next++;
	return (args);
}

t_args      *load_args(t_args *args, char **av)
{
	if (av == NULL)
		return (args);
	if (STR_EQUAL(*av, "--help"))
		return (load_args(load_no_param_arg(args), ++av));
	if (STR_EQUAL(*av, "--ip") || STR_EQUAL(*av, "--file") || \
		STR_EQUAL(*av, "--speedup"))
	{
		args = load_single_param_arg(args, av, ++av);
		return (load_args(args, av));
	}
	return (load_args(args, ++av));
}