#include <ft_nmap.h>

int     main(int ac, char ** av)
{
	t_args      args;

	(void)av;
	if (ac == 1)
		return (print_usage());
	args = MAKE_ARGS();
	print_args(&args);
	return (0);
}