# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/01/29 08:23:53 by sasiedu           #+#    #+#              #
#    Updated: 2018/01/29 08:30:57 by sasiedu          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_nmap

CC = /usr/bin/gcc

CFLAGS = -Wall -Wextra -Werror -W

RM = /bin/rm -rf

INCS = structs.h ft_nmap.h

OBJS = main.o args.o ft_nmap.o tools.o print.o

all: $(NAME)

%.o: %.c $(INCS)
	$(CC) $(CFLAGS) -c $< -o $@ -I .

$(NAME): $(OBJS)
	$(CC) $(OBJS) -o $(NAME) -lm

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all