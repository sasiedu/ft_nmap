
#ifndef FT_NMAP_H
# define FT_NMAP_H

# include <structs.h>

/*
 * print functions
 */
int             print_usage(void);
int             print_failure(int ret, char *msg);
int             print_help(void);
void            print_args(t_args *args);

/*
 * tools functions
 */
int             is_string_number(char *s);

/*
 * args functions
 */
t_args          *load_no_param_arg(t_args *args, char **av);
t_args          *load_single_param_arg(t_args *args, char **av, char **next);

#endif