#include <ft_nmap.h>

/*
 * Returns TRUE if a string is made up of 0-9 else returns FALSE
 */
int     is_string_number(char *s)
{
	if (s == NULL)
		return (FALSE);
	while (*s != '\0')
	{
		if (*s < '0' || *s > '9')
			return (FALSE);
		s++;
	}
	return (TRUE);
}